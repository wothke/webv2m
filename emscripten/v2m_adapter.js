/*
 v2m_adapter.js: Adapts "V2 by farbrausch" backend to generic WebAudio/ScriptProcessor player.
 
 version 1.1
 
 	Copyright (C) 2018-2023 Juergen Wothke

	
 note: requires SrciptNodePlayer version >= 1.2.1


 LICENSE
 
	The Artistic License 2.0 Copyright (c) 2000-2006, The Perl Foundation.
*/
class V2MBackendAdapter extends EmsHEAP16BackendAdapter {
	constructor()
	{
		super(backend_V2M.Module, 2, 
				new SimpleFileMapper(backend_V2M.Module), 
				new HEAPF32ScopeProvider(backend_V2M.Module));

		this._scopeEnabled = false;
		
		this.ensureReadyNotification();
	}
	
	enableScope(enable) 
	{
		this._scopeEnabled= enable;
	}
	
	loadMusicData(sampleRate, path, filename, data, options) 
	{
		filename = this._getFilename(path, filename);
		
		let ret = this._loadMusicDataBuffer(filename, data, ScriptNodePlayer.getWebAudioSampleRate(), 1024, this._scopeEnabled);	
		
		if (ret == 0) 
		{
			this._setupOutputResampling(sampleRate);
		} 
		return ret;			
	}
			
	getSongInfoMeta() 
	{
		return {
			title: String
		};
	}
		
	updateSongInfo(filename) 
	{
		let result = this._songInfo;
		
		let numAttr = 1;
		let ret = this.Module.ccall('emu_get_track_info', 'number');

		let array = this.Module.HEAP32.subarray(ret>>2, (ret>>2)+numAttr);
		result.title = this.Module.Pointer_stringify(array[0]);
	}
};

