// copyright (C) 2023 Juergen Wothke
//
// License Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.

window.shaders = window.shaders || {};

window.shaders.vertex = `#version 300 es
	in vec2 aVertexPosition;
	void main() {
		gl_Position = vec4(aVertexPosition, 0, 1);
	}	
`;

window.shaders.fragment = `#version 300 es
#ifdef GL_FRAGMENT_PRECISION_HIGH
	precision highp float;
#else
	precision mediump float;
#endif
	precision mediump int;
	uniform vec2 uCanvasSize;
	uniform float iTime;
	uniform float iVu;
	
	uniform sampler2D texPlastic;
	uniform sampler2D texGems;
	uniform samplerCube texCubemap;
	
	out vec4 outputColor;


	#define MAX_STEPS 100
	#define MAX_DIST 100.
	#define SURF_DIST .0005
	#define Rot(a) mat2(cos(a),-sin(a),sin(a),cos(a))
	#define antialiasing(n) n/min(iResolution.y,iResolution.x)
	#define S(d,b) smoothstep(antialiasing(1.0),b,d)
	#define B(p,s) max(abs(p).x-s.x,abs(p).y-s.y)
	#define Tri(p,s,a) max(-dot(p,vec2(cos(-a),sin(-a))),max(dot(p,vec2(cos(a),sin(a))),max(abs(p).x-s.x,abs(p).y-s.y)))
	#define DF(a,b) length(a) * cos( mod( atan(a.y,a.x)+6.28/(b*8.0), 6.28/((b*8.0)*0.5))+(b-1.)*6.28/(b*8.0) + vec2(0,11) )

	// different materials
	#define MAT_ORIG 0.
	#define MAT_MEMBRANE 1.
	#define MAT_WOOD 2.
	#define MAT_METAL 3.

	float Hash21(in vec2 p) {
		p = fract(p*vec2(234.56,789.34));
		p += dot(p,p+34.56);
		return fract(p.x+p.y);
	}

	// thx iq! https://iquilezles.org/articles/distfunctions/
	float sdTorus( in vec3 p, in vec2 t )
	{
		vec2 q = vec2(length(p.xy)-t.x,p.z);
		return length(q)-t.y;
	}

	// thx iq! https://iquilezles.org/articles/distfunctions/
	float sdBox( in vec3 p, in vec3 b )
	{
		vec3 q = abs(p) - b;
		return length(max(q,0.0)) + min(max(q.x,max(q.y,q.z)),0.0);
	}

	// thx iq! https://iquilezles.org/articles/distfunctions/
	// tweaked as the center aligned horizontal capsule. 
	float sdHorizontalCapsule( in vec3 p, in float w, in float r )
	{
	  p.x -= clamp( p.x, -w*0.5, w*0.5 );
	  return length( p ) - r;
	}


	float speaker(in vec3 p){
		vec3 prevP = p;
		float d = sdBox(p, vec3(0.45,0.95,0.34))-0.03;
		float d2 = length(p-vec3(0.,-0.2,-0.53))-0.38;
		
		d = max(-d2,d);
		
		d2 = sdTorus(p-vec3(0.,-0.2,-0.36),vec2(0.36,0.03));
		d = min(d,d2);
		
		d2 = sdTorus(p-vec3(0.,-0.2,-0.32),vec2(0.32,0.025));
		d = min(d,d2);
		d2 = length(p-vec3(0.,-0.25,-0.08))-0.12;
		d = min(d,d2);
		
		d2 = sdHorizontalCapsule(p-vec3(0.,-0.75,-0.36),0.6,0.06);
		d = max(-d2,d);
		
		d2 = length(p-vec3(0.,0.55,-0.36))-0.2;
		d = max(-d2,d);
		
		d2 = sdTorus(p-vec3(0.,0.55,-0.36),vec2(0.2,0.03));
		d = min(d,d2);
		
		p.z-=-0.36;
		p.x = abs(p.x)-0.4;
		p.y = abs(p.y)-0.9;
		d2 = length(p)-0.03;
		d = min(d,d2);
		
		return d;
	}

	float speaker2(in vec3 p){
		vec3 prevP = p;
		float d = sdBox(p, vec3(0.95,0.45,0.34))-0.03;
		float d2 = sdBox(p-vec3(0.,0.,-0.35), vec3(0.9,0.4,0.01))-0.03;
		d = max(-d2,d);
		
		p.x = abs(p.x);
		d2 = length(p-vec3(0.4,0.,-0.5))-0.36;
		d = max(-d2,d);
		
		d2 = sdTorus(p-vec3(0.4,0.,-0.3),vec2(0.34,0.03));
		d = min(d,d2);
		
		d2 = sdTorus(p-vec3(0.4,0.,-0.29),vec2(0.3,0.025));
		d = min(d,d2);
		d2 = length(p-vec3(0.45,0.,-0.08))-0.1;
		d = min(d,d2);
		
		p.z-=-0.3;
		p.x = abs(p.x)-0.86;
		p.y = abs(p.y)-0.36;
		d2 = length(p)-0.03;
		d = min(d,d2);    
		
		return d;
	}

	vec2 speaker3(in vec3 p, in float vu) {  // added meterial to return value

		float mat = MAT_WOOD; // speaker plate

		vec3 prevP = p;
		float d = sdBox(p, vec3(0.95,0.95,0.34))-0.03;
		float d0 = d;
		
		// coloring: "catch all" for most of the membrane material..    
		float d3 = length(p-vec3(0.0,0.,-0.58))-0.66;     
		if (-d3 > d) mat = MAT_MEMBRANE;
		
		// main speaker sphere
		float d2 = length(p-vec3(0.0,0.,-0.58-0.2*vu))-0.66;
		d = max(-d2,d);
	   
		d2 = sdTorus(p-vec3(0.0,0.,-0.35),vec2(0.64,0.05));
		d = min(d,d2);
		
		d2 = sdTorus(p-vec3(0.0,0.,-0.33),vec2(0.6,0.045));
		d = min(d,d2);
	   
		// center sphere
		d2 = length(p-vec3(0.0,0.,0.2-0.2*vu))-0.2;
		d = min(d,d2);
		
		// outer to inner rings..
		d2 = sdTorus(p-vec3(0.0,0.,-0.3-0.06*vu),vec2(0.56,0.035));
		
		d = min(d,d2);    
		
		d2 = sdTorus(p-vec3(0.0,0.,-0.24-0.08*vu),vec2(0.52,0.035));
		d = min(d,d2);        
			
		d2 = sdTorus(p-vec3(0.0,0.,-0.19-0.1*vu),vec2(0.47,0.035));
		d = min(d,d2);
		
		// ornaments
		d0 = d;
		
		d2 = abs(length(p.xy)-0.73)-0.07;
		d = min(d,max((abs(p.z)-0.38),d2));
		
		p.z-=-0.37;
		p.x = abs(p.x)-0.86;
		p.y = abs(p.y)-0.86;
		d2 = length(p)-0.03;
		d = min(d,d2);    

		p = prevP;
		p.z-=-0.37;
		p.xy = DF(p.xy,3.0);
		p.xy -= vec2(0.52);
		d2 = length(p)-0.03;
		d = min(d,d2); 
		
		p = prevP;
		p.xy *= Rot(radians(sin(iTime)*120.));
		p.z -= -0.37;
		p.y = abs(p.y)-0.93;
		d2 = Tri(p.xy,vec2(0.08),radians(45.));
		d = min(d,max((abs(p.z)-0.02),d2));    
		
		p = prevP;
		p.xy *= Rot(radians(90.+sin(iTime)*120.));
		p.z -= -0.37;
		p.y = abs(p.y)-0.93;
		d2 = Tri(p.xy,vec2(0.08),radians(45.));
		d = min(d,max((abs(p.z)-0.02),d2));      

		// coloring: ornaments 
		if (d < d0) mat = MAT_METAL;

		return vec2(d, mat);
	}

	vec2 changeSpeakers(in vec3 p, in float start, in float speed, in float vu) {
		vec3 prevP = p;
		float endTime = 3.;
		float t = iTime*speed;
		float scenes[3] = float[](0.,1.,2.);
		for(int i = 0; i<scenes.length(); i++){
			scenes[i] = mod(scenes[i]+start,endTime);
		}
		
		float scene = scenes[int(mod(t,endTime))];
		
		float d = 10.;
		if(scene<1.) {
			p.x = abs(p.x)-0.5;
			d = speaker(p);
	//    } else if (scene >= 1. && scene<2.){
		} else if (scene<2.){
			p.y = abs(p.y)-0.5;
			d = speaker2(p);
		} else {
			return speaker3(p, vu);
		}
		
		return vec2(d, MAT_ORIG);
	}

	vec2 GetDist(vec3 p, in float vu) {
		vec3 prevP = p;
		
		p.y -=iTime*0.5;
		vec2 id = floor(p.xy*0.5);
		p.z -= 3.;
		p.xy = mod(p.xy,2.0)-1.0;

		id *= .5;
		float rand = Hash21(id);
		
		float d = 10.;
		p.z -= rand*0.3;
		if(rand<0.3) {
			return changeSpeakers(p,1.,0.5+rand, vu);
		} else if(rand>=0.3 && rand<0.7) {
			return speaker3(p, vu);
		} else {
			p.x = abs(p.x)-0.5;
			d = speaker(p);
		}
		
		return vec2(d, MAT_ORIG);
	}

	vec2 RayMarch(vec3 ro, vec3 rd, float side, int stepnum, in float vu) {
		vec2 dO = vec2(0.0);
		
		for(int i=0; i<stepnum; i++) {
			vec3 p = ro + rd*dO.x;
			vec2 dS = GetDist(p, vu);
			dO.x += dS.x*side;
			dO.y = dS.y;
			
			if(dO.x>MAX_DIST || abs(dS.x)<SURF_DIST) break;
		}
		
		return dO;
	}

	vec3 GetNormal(in vec3 p, in float vu) {
		float d = GetDist(p, vu).x;
		vec2 e = vec2(.001, 0);
		
		vec3 n = d - vec3(
			GetDist(p-e.xyy, vu).x,
			GetDist(p-e.yxy, vu).x,
			GetDist(p-e.yyx, vu).x);
		
		return normalize(n);
	}

	vec3 R(in vec2 uv, in vec3 p, in vec3 l, in float z) {
		vec3 f = normalize(l-p),
			r = normalize(cross(vec3(0,1,0), f)),
			u = cross(f,r),
			c = p+f*z,
			i = c + uv.x*r + uv.y*u,
			d = normalize(i-p);
		return d;
	}

	// https://www.shadertoy.com/view/3lsSzf
	float calcOcclusion( in vec3 pos, in vec3 nor, in float vu)
	{
		float occ = 0.0;
		float sca = 1.0;
		for( int i=0; i<4; i++ )
		{
			float h = 0.01 + 0.15*float(i)/4.0;
			vec3 opos = pos + h*nor;
			float d = GetDist( opos, vu ).x;
			occ += (h-d)*sca;
			sca *= 0.95;
		}
		return clamp( 1.0 - 2.0*occ, 0.0, 1.0 );
	}

	vec3 diffuseMaterial(in vec3 n, in vec3 rd, in vec3 p, in vec3 col, in vec3 sky, in vec3 bounce, in float spec, in float vu) {
		float occ = calcOcclusion(p,n, vu);
		vec3 diffCol = vec3(0.0);
		vec3 lightDir = normalize(vec3(1,10,-10));
		float diff = clamp(dot(n,lightDir),0.0,1.0);
		float shadow = step(RayMarch(p+n*0.3,lightDir,1.0, 15, vu).x,0.9);
		float skyDiff = clamp(0.5+0.5*dot(n,vec3(0,1,0)),0.0,1.0);
		float bounceDiff = clamp(0.5+0.5*dot(n,vec3(0,-1,0)),0.0,1.0);
		diffCol = col*vec3(-0.5)*diff*shadow*occ;
		diffCol += col*sky*skyDiff*occ;
		diffCol += col*bounce*bounceDiff*occ;
		diffCol += col*pow(max(dot(rd, reflect(lightDir, n)), 0.0), spec)*occ; // spec
			
		return diffCol;
	}

	vec3 metalMaterial(in vec3 n, in vec3 rd, in vec3 p) {
		// version based on cubemap that uses the same "gems" image an all sides
		vec3 t= texture(texGems, vec2(p.y - iTime*.5, p.x)).xyz; // "undo" y-movement
		n+= t* .2;

		vec3 c = t; //vec3(1);
		// simply add some parts of the normal to the color
		// gives impression of 3 lights from different dir with different color temperature
		c += n.xyz*.11+.05;

		//  reflection of cubemap
		vec3 R = p-2.0*dot(p,n.xyz)*n.xyz;		
		vec3 cub= texture(texCubemap,R).xyz;
		
		n+= cub * .3;
		
		c = c * cub * 1.6 + 0.2;

//		c = cub;	// nice: brass with green and blue reflections	
		
		return c;
	}
	vec3 metalMaterialXXX(in vec3 n, in vec3 rd, in vec3 p) {
		// version based on colorful generated cubemap
		vec3 t= texture(texGems, vec2(p.y - iTime*.5, p.x)).xyz; // "undo" y-movement
		n+= t* .2;

		vec3 c = t; //vec3(1);
		// simply add some parts of the normal to the color
		// gives impression of 3 lights from different dir with different color temperature
		c += n.xyz*.11+.05;

		//  tint
	//	c+=vec3(.2, .172, 0.3);

		//  reflection of cubemap
		vec3 R = p-2.0*dot(p,n.xyz)*n.xyz;
		c *= texture(texCubemap,R).xyz + .3;

		return c;
	}

	vec3 materials(int mat, vec3 n, vec3 rd, vec3 p, vec3 col, float vu)
	{
		vec3 sky, bounce; float spec;
		
		if( mat<=int(MAT_WOOD) )
		{
			if( mat==int(MAT_ORIG) )
			{
				col=vec3(1.3);
				sky=vec3(1.0,1.0,0.9);
				bounce=vec3(0.3,0.3,0.3);
				spec=60.;
			}
			else if( mat==int(MAT_MEMBRANE) )
			{
				col=vec3(0.5);
				sky=vec3(0.2,0.2,0.2);
				bounce=vec3(0.1,0.1,0.1);
				spec=30.;
			}
			else //if( mat==int(MAT_WOOD) )
			{
				col = texture(texPlastic, vec2(p.y - iTime*.5, p.x)/2.).xyz; // "undo" y-movement     
				sky=vec3(0.8,0.8,0.7);
				bounce=vec3(0.1,0.1,0.1);
				spec=30.;
			}
			return diffuseMaterial(n, rd, p, col, sky, bounce, spec, vu);
		}
		else
		{
			return metalMaterial(n, rd, p);
		}
	}


	void main() {
		vec2 uv = (gl_FragCoord.xy / uCanvasSize.xy) * 2.0 - vec2(1.0,1.0);					 
		vec2 prevUV = uv;
		float scene = mod(iTime,15.);
		float rotY = -10.;
		float rotX = 0.;
		if(scene>=5. && scene<10.){
			rotY = 0.;
			rotX = -30.;
		} else if(scene>=10.){
			rotY = 0.;
			rotX = 30.;
		}
		
		vec3 ro = vec3(0, 0, -1.5);
		ro.yz *= Rot(radians(rotY));
		ro.xz *= Rot(radians(rotX));
			
		float vu = iVu; // used to position speaker membrane (obviously this is NOT what a real speaker does..)		
		
		vec3 rd = R(uv, ro, vec3(0,0.0,0), 1.0);
		vec2 d = RayMarch(ro, rd, 1.,MAX_STEPS, vu);
		vec3 col = vec3(.0);
		
		if(d.x<MAX_DIST) {
			vec3 p = ro + rd * d.x;
					
			vec3 n = GetNormal(p, vu);
			int mat = int(d.y);
			col = materials(mat,n,rd,p,col, vu);
		}
		col = pow( col, vec3(0.9545) );    // gamma correction

		outputColor = vec4(sqrt(col),1.0);
	}
`;
