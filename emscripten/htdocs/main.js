
var songs = [
		"V2/Dafunk/breeze.v2m",
		"V2/Little Bitchard/fr-037 the code inside.v2m",
		"V2/Quickyman/outphatmix introtune.v2m",
		"V2/Quickyman/projone.v2m",
		"V2/Paniq/maschinenzeit.v2m",
		"V2/Quickyman/depeche mode - precious.v2m",
		"V2/Izard/antiphore.v2m",
		"V2/Quickyman/arcane remix.v2m",
		"V2/Dalezy/12 dolls for a december moon.v2m",
		"V2/Quickyman/tristar - amiga classix - classic tristar boulderdemo.v2m",
		"V2/Quickyman/movietrack - mortal kombat.v2m",
		"V2/Dafunk/thesis.v2m",
	];


class Main {
	constructor()
	{
		this._backend;
		this._playerWidget;
		this._scopesWidget;

		this._enableWEBGL = undefined;
	}

	_setupSliderHoverState()
	{
		// hack: add "highlight while dragging" feature for all sliders on the page
		// (this should rather be setup locally upon creation of the sliders..)

		let slider= $(".slider a.ui-slider-handle");
		slider.hover(function() {
				$(this).prev().toggleClass('hilite');
			});
		slider.mousedown(function() {
				$(this).prev().addClass('dragging');
				$("*").mouseup(function() {
					$(slider).prev().removeClass('dragging');
				});
			});
	}

	_getVuMeterLevel()
	{
		return (typeof this._tracer == 'undefined') ? 0 : this._tracer.getOverallVuMeterLevel();
	}

	_draw()
	{
		gl.uniform2f(this.program.uCanvasSize, c.width, c.height);

		let time = performance.now() / 1000;	// secs
		time = time % 900;						// reset every 15 minutes

		gl.uniform1f(this.program.iTime, time);

		let vu = this._getVuMeterLevel()
		gl.uniform1f(this.program.iVu, vu);

		gl.uniform1i(this.program.texPlastic, 0);
		gl.uniform1i(this.program.texGems, 1);
		gl.uniform1i(this.program.texCubemap, 2);

		gl.drawArrays(gl.TRIANGLE_STRIP, 0, this.vertexPosBuffer.numItems);

		requestAnimationFrame(this._draw.bind(this))
	}

	_startAnimation()
	{
		if (useWEBGL())
		{
			this.vertexPosBuffer = screenQuad();

			let program = setupProgram(window.shaders.vertex, window.shaders.fragment);
			gl.useProgram(program);

			program.vertexPosAttrib = gl.getAttribLocation(program, 'aVertexPosition');

			program.uCanvasSize = gl.getUniformLocation(program, 'uCanvasSize');
			program.iTime = gl.getUniformLocation(program, 'iTime');
			program.iVu = gl.getUniformLocation(program, 'iVu');
			program.texPlastic = gl.getUniformLocation(program, "texPlastic");
			program.texGems = gl.getUniformLocation(program, "texGems");
			program.texCubemap = gl.getUniformLocation(program, "texCubemap");

			gl.enableVertexAttribArray(program.vertexPosAttrib);
			gl.vertexAttribPointer(program.vertexPosAttrib, this.vertexPosBuffer.itemSize, gl.FLOAT, false, 0, 0);

			this.program = program;

			let p1 = loadTexture("speaker.webp");
			let p2 = loadTexture("gems.webp");
			let p3 = loadCubemap("gems.webp", "gems.webp", "gems.webp", "gems.webp", "gems.webp", "gems.webp");

			Promise.all([p1, p2, p3]).then((result) => {

				// page uses static mapping of texture units that can be set here
				activateTexture2d(0, result[0]);
				activateTexture2d(1, result[1]);
				activateTextureCubeMap(2, result[2]);

				this._draw();
			})
			.catch(err => {
				console.log(err);
			});
		}
	}

	_doOnTrackEnd()
	{
		this._playerWidget.playNextSong();
	}

	run()
	{
		this._startAnimation();

		this._tracer = new ChannelStreamer();
		this._tracer.setZoom(1);

		// note: with WASM this may not be immediately ready
		this._backend = new V2MBackendAdapter();
		this._backend.setProcessorBufSize(2048*2);


		ScriptNodePlayer.initialize(this._backend, this._doOnTrackEnd.bind(this), [], false, this._tracer)
		.then((msg) => {

			// constructor requires funtional backend
			this._scopesWidget = new OscilloscopesWidget("scopesContainer", this._tracer, this._backend);


			let optionsParser = function(someSong) {
									let arr = someSong.split(";");
									if (arr.length > 1) someSong = arr[0];
									let track = arr.length > 1 ? parseInt(arr[1]) : -1;
									let options = {};
									options.track = track;

									let isLocal = someSong.startsWith("/tmp/") || someSong.startsWith("music/");
									someSong = isLocal ? someSong : window.location.protocol + "//ftp.modland.com/pub/modules/" + someSong;

									return [someSong, options];
								};

			let defaultSongIdx = -1;
			this._playerWidget = new PlaylistPlayerWidget("playerContainer", songs, this._scopesWidget.onSongChanged.bind(this._scopesWidget),
															false, true, optionsParser, 0, defaultSongIdx, {});

			this._setupSliderHoverState();

			this._playerWidget.playNextSong();
		});
	}
}
