/*
* This file adapts "Farbrausch V2M player" to the interface expected by my generic JavaScript player..
*
* Copyright (C) 2018-2023 Juergen Wothke
*
* Credits:   https://github.com/jgilje/_v2m-player
*
* License: This WebAudio port of the V2M lib uses the same license as the original code,
*          i.e. The Artistic License 2.0 (see separate LICENSE file)
*/


/*
	known issues: "kZ - Glary Utilities 2.x.x.x crk._v2m": for some reason sound
	overamplifies (seems to be linked to activated BUG_V2_FM_RANGE bug)
*/
#include <emscripten.h>
#include <stdio.h>
#include <stdlib.h>

#include <iostream>
#include <fstream>

#include <sounddef.h>
#include <v2mplayer.h>
#include <v2mconv.h>
#include <scope.h>


#include "MetaInfoHelper.h"
using emsutil::MetaInfoHelper;

#define CHANNELS 2
#define BYTES_PER_SAMPLE 2

namespace v2m {

	class Adapter {
	public:
		Adapter() : _bufferConverted(0), _sampleBufferSize(0), _synthBuffer(NULL) , _sampleBuffer(NULL),
					_sampleRate(0), _samplesAvailable(0), _sampleTime(0), _nullScope(NULL)
		{
		}

		int loadFile(char *filename, void *inBuffer, uint32_t inBufSize, uint32_t sampleRate, uint32_t audioBufSize, unsigned char scopesEnabled)
		{
			_sampleRate = sampleRate;

			scope_activated= scopesEnabled;

			allocBuffers(audioBufSize);

			if (inBufSize < 1000) return 1;	// just some file not found message (todo: catch in player.)

			sdInit();

			inBuffer = getBufferConverted(inBuffer, inBufSize);

			_v2m.Init();

			if (inBuffer && _v2m.Open((uint8_t *)inBuffer, _sampleRate))
			{
				_v2m.Play();

				std::string title = filename;
				
				int i = title.find_last_of( '/' );
				if (i >= 0)
				{
					title = title.substr(i+1);
				}
				
				title.erase( title.find_last_of( '.' ) );	// remove ext

				MetaInfoHelper *info = MetaInfoHelper::getInstance();
				info->setText(0, title.c_str(), "");

				return 0;
			}
			return 1;
		}

		void teardown()
		{
			MetaInfoHelper::getInstance()->clear();

			if (_bufferConverted) {
				delete _bufferConverted;		// allocated using "new" (see ConvertV2M)
				_bufferConverted = 0;
			}

			sdClose();

			_v2m.Close();	// redundant: already used in new Open

			_sampleTime = 0;
		}

		int getSampleRate()
		{
			return _sampleRate;
		}

		int genSamples() {
			if (getCurrentPosition() >= getMaxPosition()) {
				_samplesAvailable = 0;
				return 1;
			}

			_v2m.Render(_synthBuffer, _sampleBufferSize);
			_samplesAvailable = _sampleBufferSize;

			// convert float to int16 /(just so the player can be merged more easily with others)
			for (int i= 0; i < _samplesAvailable*CHANNELS; i++) {
				double v = 0x8000 * (double)_synthBuffer[i];
				if (v < -32760) v = -32760;
				if (v > 32760) v = 32760;

				_sampleBuffer[i] = (int16_t)v;
			}

			_sampleTime += _samplesAvailable;
			return 0;
		}

		char* getSampleBuffer() {
			return (char*)_sampleBuffer;
		}

		long getSampleBufferLength() {
			return _samplesAvailable;
		}

		int getCurrentPosition() {
			return _sampleTime / getSampleRate() * 1000;
		}
		void seekPosition(int ms) {
			_v2m.Play(ms);
			_sampleTime = ms * getSampleRate() / 1000;
		}
		int getMaxPosition() {
			return _v2m.Length() * 1000; // in ms
		}

		int getNumberTraceStreams() {
			return 16;
		}
		
		const char** getTraceStreams() {
			// issue: the scope buffers do not seem to always be properly
			// unitialized (e.g. when voice goes mute)

			const char**bufs = (const char**)get_scope_buffers();
			for (int i= 0; i < getNumberTraceStreams(); i++) {
				if (bufs[i] == NULL) {
					bufs[i] = (const char*)_nullScope;	// make sure there is always a valid buffer
				}
			}
			return bufs;
		}

	private:
		void * getBufferConverted(const void * inBuffer, size_t inBufSize)
		{
			int version = CheckV2MVersion((unsigned char*)inBuffer, inBufSize);
			if (version < 0)
			{
				return NULL;		// fatal error
			}

			int convertedLength;
			ConvertV2M((const unsigned char*)inBuffer, inBufSize, &_bufferConverted, &convertedLength);

			return _bufferConverted;
		}

		void allocBuffers(uint32_t audioBufSize)
		{
			if (_sampleBufferSize != audioBufSize)
			{
				if (_synthBuffer) free(_synthBuffer);
				if (_sampleBuffer) free(_sampleBuffer);
				if (_nullScope) free(_nullScope);


				_sampleBufferSize = audioBufSize;
				_synthBuffer = (float *)malloc(_sampleBufferSize * CHANNELS * sizeof(float));
				_nullScope = (float *)malloc(_sampleBufferSize * CHANNELS * sizeof(float));
				_sampleBuffer = (int16_t *)malloc(_sampleBufferSize * CHANNELS*sizeof(int16_t));
			}
		}

	private:
		unsigned char* _bufferConverted;

		uint32_t _sampleBufferSize;
		float *_synthBuffer;
		int16_t *_sampleBuffer;
		uint32_t _sampleRate;

		int _samplesAvailable;
		long _sampleTime;

		float *_nullScope;

		V2MPlayer _v2m;
	};
};

v2m::Adapter _adapter;



// old style EMSCRIPTEN C function export to JavaScript.
// todo: code might be cleaned up using EMSCRIPTEN's "new" Embind feature:
// https://emscripten.org/docs/porting/connecting_cpp_and_javascript/embind.html
#define EMBIND(retType, func)  \
	extern "C" retType func __attribute__((noinline)); \
	extern "C" retType EMSCRIPTEN_KEEPALIVE func


EMBIND(int, emu_load_file(char *filename, void * inBuffer, uint32_t inBufSize, uint32_t sampleRate, uint32_t audioBufSize, uint32_t scopesEnabled)) {
	return _adapter.loadFile(filename, inBuffer, inBufSize, sampleRate, audioBufSize, scopesEnabled); }
EMBIND(void, emu_teardown())						{ _adapter.teardown(); }
EMBIND(int, emu_get_sample_rate())					{ return _adapter.getSampleRate(); }
EMBIND(int, emu_set_subsong(int track))				{ return 0;	/*there are no subsongs*/ }
EMBIND(const char**, emu_get_track_info())			{ return MetaInfoHelper::getInstance()->getMeta(); }
EMBIND(int, emu_compute_audio_samples())			{ return _adapter.genSamples(); }
EMBIND(char*, emu_get_audio_buffer())				{ return _adapter.getSampleBuffer(); }
EMBIND(int, emu_get_audio_buffer_length())			{ return _adapter.getSampleBufferLength(); }
EMBIND(int, emu_get_current_position())				{ return _adapter.getCurrentPosition(); }
EMBIND(void, emu_seek_position(int ms))				{ _adapter.seekPosition(ms);}
EMBIND(int, emu_get_max_position())					{ return _adapter.getMaxPosition(); }
EMBIND(int, emu_number_trace_streams())				{ return _adapter.getNumberTraceStreams(); }
EMBIND(const char**, emu_get_trace_streams())		{ return _adapter.getTraceStreams(); }
EMBIND(const char**, emu_get_trace_titles())		{ return 0; }

